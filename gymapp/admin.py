from django.contrib import admin
from gymapp.models import Contact,Membershipplan,Enrollment,Trainer,Gallery,Attendance
from .models import Carousel


# Register your models here.
admin.site.register(Contact)
admin.site.register(Membershipplan)
admin.site.register(Enrollment)
admin.site.register(Trainer)
admin.site.register(Gallery)
admin.site.register(Carousel)
admin.site.register(Attendance)
